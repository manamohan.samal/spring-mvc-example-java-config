<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1 style="text-align: center; color: red;">Welcome to My spring
		MVC application</h1>
	<h3 style="text-align: center; color: blue;">
		The time is :<%=java.util.Calendar.getInstance().getTime()%></h3>

	<div>
		<button onClick="location.href='./welcome'">Home</button>
		<button onclick="location.href='./aboutus'">About Us</button>
		<button onclick="location.href='./products'">Products</button>
		<button onclick="location.href='./contact'">Contact</button>
	</div>
</body>
</html>