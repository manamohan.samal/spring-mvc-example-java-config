<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="text-align:center;">
	<h1>---You are in the products page.---</h1>
	<h3>Here are some dummy data:</h3>
	<table align="center" border="1">
		<tr>
			<th>Sl.</th>
			<th>Product</th>
			<th>Price</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Moblie</td>
			<td>12000</td>
		</tr>
		<tr>
			<td>2</td>
			<td>TV</td>
			<td>25000</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Tablet</td>
			<td>12000</td>
		</tr>
		<tr>
			<td>4</td>
			<td>PC</td>
			<td>1230000</td>
		</tr>
	</table>
</body>
</html>