package com.accenture.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/welcome")
	public String welcome() {
		System.out.println("Inside welcome()");
		return "/home.jsp";
	}
	
	@RequestMapping("/aboutus")
	public String aboutUs() {
		System.out.println("HomeController.aboutUs()");
		return "/aboutus.jsp";
	}
	
	@RequestMapping("/contact")
	public String contact() {
		System.out.println("HomeController.contact()");
		return "/contact.jsp";
	}
}
