package com.accenture.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductController {

	@RequestMapping("/products")
	public String productsView() {
		System.out.println("ProductController.productsView()");
		return "/products.jsp";
	}
}
